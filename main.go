package main

import (
	"context"
	"fmt"
	clisv1 "gitee.com/crystal-hz/packagecheck/pkg/apis/packagecheck/v1"
	clientSet "gitee.com/crystal-hz/packagecheck/pkg/generated/clientset/versioned"
	"io"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/yaml"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/tools/clientcmd"
	"log"
	"os"
)

func main() {
	//创建连接kube-apiserver配置文件
	clientConfig, err := clientcmd.BuildConfigFromFlags("", "/root/.kube/config")
	if err != nil {
		panic(err)
	}
	//初始化所有客户端连接
	damClient, err := clientSet.NewForConfig(clientConfig)
	if err != nil {
		panic(err)
	}
	//get, err := damClient.PackagecheckV1().PackageChecks("default").Get(context.Background(), "hello", metav1.GetOptions{})
	//if err != nil {
	//	log.Fatal(err)
	//	return
	//}

	//创建crd
	dyanmicClient, err := dynamic.NewForConfig(clientConfig)
	if err != nil {
		log.Fatal(err)
		return
	}
	file, err := os.Open("./pack.yaml")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer file.Close()
	all, err := io.ReadAll(file)
	if err != nil {
		log.Fatal(err)
		return
	}
	decoder := yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme)
	obj := &unstructured.Unstructured{}
	_, gvk, err := decoder.Decode(all, nil, obj)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(obj.GetName(), gvk.String())
	create, err := dyanmicClient.Resource(schema.GroupVersionResource{
		Group:    "apiextensions.k8s.io",
		Version:  "v1",
		Resource: "customresourcedefinitions",
	}).Create(context.Background(), obj, metav1.CreateOptions{})
	if err != nil {
		log.Fatal("hello", err)
		return
	}
	fmt.Println(create)
	//fmt.Printf("%+v\n", get)
	fmt.Println("创建crd成功")
	//创建cr
	cr, err := os.Open("./packs.yaml")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer cr.Close()
	readAll, err := io.ReadAll(cr)
	if err != nil {
		log.Fatal(err)
		return
	}
	pc := &clisv1.PackageCheck{}
	_, s, err := decoder.Decode(readAll, nil, pc)
	if err != nil {
		log.Fatal(err)
		return
	}
	fmt.Println(s.GroupVersion())
	check, err := damClient.PackagecheckV1().PackageChecks("default").Create(context.Background(), pc, metav1.CreateOptions{})
	if err != nil {
		log.Fatal(err)
		return
	}
	fmt.Println(check)
}
